//Format to Save user/customer data into text file
//MobileNo&*&FIRSTNAME&*&LASTNAME&*&EMAIL&*&DOB&*&CREDITS&*&ADDRESS&*&EXP&*&ANIVERDATE
//0       //1        //2       	//3      //4   	//5          //6   //7        //8   

//Format to Save transaction history data into text file
//MobileNo&*&SERVICE&*&CREDITS&*&TYPE&*&DATE

//Formate to Save service data into text file
//NAME&*&CREDIT_POINT

//user/customer seperator
var rowSep 		= "&*$*&";
var fieldSep 	= '&*&';

window.currentAction = "";
window.currentCardId = "";
window.editServiceFlag = false;
window.currentTotalCredit = 0;
window.otp = 0;

//Array to store existing data
var serviceArray 	= [];
var cardArray 		= [];
var customerArray 	= [];

//Filesystem
window.requestFileSystem  = window.requestFileSystem || window.webkitRequestFileSystem;

//Here is the code to save things into file through file system
var appFS = null;
var requestStorage 		= 1024 * 1024 * 16;
var customerFolder 		= "o_customers";
var customerFile 		= "o_customers.txt";
var serviceFolder 		= "o_services";
var serviceFile 		= "o_services.txt";
var transactionFolder 	= "o_transactions";
var transactionFile 	= "o_transactions.txt";

//Today's Date
var today 	= new Date();
var dd 		= String(today.getDate()).padStart(2, '0');
var mm 		= String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
var yyyy 	= today.getFullYear();
//var currentDate = today.getDate()+'-'+(today.getMonth()+1)+'-'+today.getFullYear();
var currentDate = dd + '-' + mm + '-' + yyyy;
console.log(currentDate);

//initialize file system object
function onInitFs(fs) {
	appFS = fs;
}
// FileSystem Error Handler
function fsErrorHandler(e) {
    var msg = 'An error occured:';
    console.log('error name = ', e.name);

    switch (e.name) {
        case 'QuotaExceededError':
            msg += 'QUOTA_EXCEEDED_ERR';
            break;
        case 'NotFoundError':
            msg += 'NOT_FOUND_ERR';
            break;
        case 'SecurityError':
            msg += 'SECURITY_ERR';
            break;
        case 'InvalidModificationError':
            msg += 'INVALID_MODIFICATION_ERR';
            break;
        case 'InvalidStateError':
            msg += 'INVALID_STATE_ERR';
            break;
        default:
            msg += 'Unknown Error';
            break;
    }
    console.log('Error: ' + msg);
}

setTimeout(function(){ 
	// FileSystem RequestQutota
	navigator.webkitPersistentStorage.requestQuota(
	    requestStorage,
	    function (grantedBytes) {
	        window.requestFileSystem(PERSISTENT, grantedBytes, onInitFs, fsErrorHandler);
	        // console.log('request file system storage');
     
	    },
	    function (e) {
	        console.log('Error', e);
	    }
	);
}, 100);

$(document).ready(function(){

	$('body').on('click','#logout',function(){
		localStorage.removeItem('login');
		localStorage.removeItem('pass');
		location.reload();
	});

	if(localStorage.getItem('business_name')){
		var businessName = localStorage.getItem('business_name');
		var businessMobile = localStorage.getItem('business_mobile');
		var businessAddress =localStorage.getItem('business_address');

		$("#business_name").val(businessName);
		$("#business_mobile").val(businessMobile);
		$("#business_address").val(businessAddress);
		$('.logo').html("<span>"+businessName+"</span>");
	}
					

	$( "#expDate" ).datepicker({ dateFormat: 'dd-mm-yy',changeMonth:true,changeYear:true });
    $( "#birthDate" ).datepicker({ dateFormat: 'dd-mm-yy',changeMonth:true,changeYear:true });
    $( "#aniversaryDate" ).datepicker({ dateFormat: 'dd-mm-yy',changeMonth:true,changeYear:true });

	//Custom validation rules
	$.validator.addMethod('unique_value', function(value, element) {
        	
		if( typeof  serviceArray[value] !== 'undefined' && serviceArray[value].length > 0 ) {

			if( window.editServiceFlag == value ){
				return true;
			} else {
				return false;
			}
		} else {
			return true;
		}

    }, 'This service already exists!');

    //custom validation rule for adding customer
    $.validator.addMethod('unique_value_card', function(value, element) {
        	
		if( typeof  customerArray[value] !== 'undefined' && customerArray[value].length > 0 ) {
			$("#sendOTP").attr('disabled',true);
			return false;
		} else {
			$("#sendOTP").attr('disabled',false);
			return true;
		}

    }, 'This mobile number already exists!');

    $.validator.addMethod('lessThanEqual', function(value, element) {
    // console.log('validator',window.currentTotalCredit,value);
    // console.log('type of value',typeof(window.currentTotalCredit));
    if(parseInt(window.currentTotalCredit) >= parseInt(value)){
    	// console.log("remaining is greater")
    	return true;
    }else{
    	return false;
    }
}, "The deduct credit must be less than Remaining Credit");

	//Putting correct image source into loader
	var loaderUrl = chrome.extension.getURL("img/loader.gif");
	$('.loader').attr('src', loaderUrl);
	//$('.backup_loader').attr('src', loaderUrl);

 	// var customerTable = $('#customerList').DataTable({dom:'Bfrtip',
  //       buttons: ['excel']});

 	var customerTable = $('#customerList').DataTable( {
        dom: 'Bfrtip',
        buttons: [
            {
                extend: 'excel',
                filename: 'customer-'+currentDate,
                text: 'Export Customers'
            }
            
        ]
    } );
 	var serviceTable = $('#serviceList').DataTable( {
        dom: 'Bfrtip',
        buttons: [
            {
                extend: 'excel',
                filename: 'service-'+currentDate,
                text: 'Export Services'
            }
            
        ]
    } );
 	var transactionTable = $('#transactionList').DataTable( {
        dom: 'Bfrtip',
        buttons: [
            {
                extend: 'excel',
                filename: 'transaction-'+currentDate,
                text: 'Export Transaction'
            }
            
        ]
    } );
 	var customerTransactionTable = $('#customerTransactionList').DataTable( {
        dom: 'Bfrtip',
        buttons: [
            {
                extend: 'excel',
                filename: 'customerTrxn-'+currentDate,
                text: 'Export Transaction'
            }
            
        ]
    } );
	//check if Admin login or not
 	var getLogin = localStorage.getItem('login');
 	var getPass  = localStorage.getItem('pass');

 	// $.getJSON('../js/credentials.json', function(resp) { 
 		if(window.login === getLogin && window.pass === getPass){
 			$("#dashboardScreen").show();
 			$(".main-dashboard").show();
 			$("#homearea").hide();
 			$("#login_screen").hide();
 		}
 	// });


 	//Get customer data from the file
 	$('body').on('click', '#load_customer_data', function(){

 		var cardId = $.trim($('#credit_screen_card_id').val());
 		if( cardId != "" ) {

 			window.currentAction = 'return_entry';
 			window.currentCardId = cardId;

 			$('.credit_screen_customer_detail').hide();
 			$('.ui_blocker_credit_screen').show();

 			readWriteCustomerEntry(customerFolder, customerFile, "", true, false,false, creditScreenData);

 		}

 	});
 	//End of function to get data for customer from file

 	//get individual customer Transaction data start
 		$('body').on('click', '#individual_customer_data', function(){

 		var cardId = $.trim($('#individual_card_id').val());
 		if( cardId != "" ) {

 			window.currentCardId = cardId;
 			$('#trans_customer_card').text(window.currentCardId);
 			if(window.customerArray[window.currentCardId]){
		  		var customerRow = window.customerArray[window.currentCardId].split(fieldSep);

		  		$('#trans_customer_name').text(customerRow[1]+" "+customerRow[2]);

		  		$('#trans_customer_credit').text(customerRow[5]);

	 			readWriteTransactionEntry(transactionFolder, transactionFile, "", true, false, writeDoneCustomerTrasactionCallback);

	 			$("#serviceScreen").hide();
		 		$("#dashboardScreen").hide();
		 		$("#transactionScreen").hide();
		 		$("#individualCustomer").modal('hide');;
		 		$("#customerTransactionScreen").show();
		 	}else{
		 		$('.ui_blocker_credit_screen').hide();
				$('.ui_blocker_credit_screen_error').show().html('No records found!!');
		 	}
 		}

 	});
 	//get individual customer Transaction data end

 	$('body').on('click', '#individual_customer', function(){
 		$('#individual_card_id').val('');
 		$('.ui_blocker_credit_screen_error').hide();
 	});

 	//Get customer data from the file for recharge
 	$('body').on('click', '#load_customer_for_recharge', function(){

 		var cardId = $.trim($('#recharge_screen_card_id').val());
 		if( cardId != "" ) {

 			var card1 = cardId.replace(';','');
			var card2 =	card1.replace('?','');
			   	cardId = card2;
			   	console.log('load_customer_data card id',cardId);

 			window.currentAction = 'return_entry';
 			window.currentCardId = cardId;
 			// $('#credit_card_id').val(cardId);

 			$('.recharge_screen_customer_detail').hide();
 			$('.ui_blocker_credit_screen').show();

 			readWriteCustomerEntry(customerFolder, customerFile, "", true, false,false, rechargeScreenData);

 		}

 	});
	 	//End of function to get data for customer from file

 	//Fill credit points when service changed from dropdown
 	$('body').on('change', '#service_taken', function(){
 		$('#deduct_credit').val(serviceArray[$(this).val()]);
 	});

	//Login form show and submit
 	$("#show_login").click(function(){
 		$("#login_screen").show();
 		$("#homearea").hide();
 	});

 	$("#submit").click(function(){
 		var ulogin 	= $("#login").val();
 		var upass 	= $("#password").val();
 		//var licensekey 	= window.license;

 		if(window.login  === ulogin && window.pass === upass){

 			
			localStorage.setItem('login', ulogin);
			localStorage.setItem('pass', upass);
			$("#response").addClass("alert-success").html("Login successful, Redirecting to Dashboard")
			$("#response").removeClass("alert-danger");
			$(".main-dashboard").show();
			$("#dashboardScreen").show();
			$("#homearea").hide();
			$("#login_screen").hide();
		}else{
			$("#response").addClass("alert-danger").html("Please use correct credentials to login!!")
			$("#response").removeClass("alert-success");
		}
 		
 		// $.getJSON('../js/credentials.json', function(jd) { 
 		// 	$.ajax({
			// 	type:'post',
			// 	url:"http://vkaps.com/chrome_extension_backup/license_validate.php",
			// 	data: {"data_type":"licensekey","key":licensekey},
			// 	error: function(jqXHR) { 
			//     if(jqXHR.status==0) {
			//             $('#response').html("There is weak or No internet connection, Please try again!!").css('color','red');
			//         }
			//     },
			// 	success:function(respData) {	
			// 		if(respData == 'success'){

			// 			if(window.login  === ulogin && window.pass === upass){
			//  				console.log("login works")
			//  				localStorage.setItem('login', ulogin);
			//   				localStorage.setItem('pass', upass);
			//  				$("#response").addClass("alert-success").html("Login successful, Redirecting to Dashboard")
			//  				$("#response").removeClass("alert-danger");
			//  				$(".main-dashboard").show();
			//  				$("#dashboardScreen").show();
			//  				$("#homearea").hide();
			//  				$("#login_screen").hide();
			 				
			//  			}else{
			//  				$("#response").addClass("alert-danger").html("Please use correct credentials to login!!")
			//  				$("#response").removeClass("alert-success");
			//  			}
						
			// 		}else{
			// 			$("#response").addClass("alert-danger").html("License Key is invalid")
			// 		}
			// 	}
			// });

 			
 		// });
		
 	});

 //To create invoice or charge for service
 	$(".showCustomer").click(function(){
 		$("#dashboardScreen").show();
 		$("#transactionScreen").hide();
 		$("#serviceScreen").hide();
 		$("#customerTransactionScreen").hide();
 		
 	});

//Service List table show
 	$(".services").click(function(){
 		$("#serviceScreen").show();
 		$("#dashboardScreen").hide();
 		$("#transactionScreen").hide();
 		$("#customerTransactionScreen").hide();
 	});

//Service List table show
 	$("#recentHistory").click(function(){
 		$("#serviceScreen").hide();
 		$("#dashboardScreen").hide();
 		$("#transactionScreen").show();
 		$("#customerTransactionScreen").hide();
 	});



 	//Global function to store all user/customer data into filesystem table
 	function readWriteCustomerEntry(folder, filename, filedata, readonly, update,deletestatus, callback){

 		// Create Folder
	    appFS.root.getDirectory(folder, { create: true }, function (dirEntry) {
	    	// Write settings to File System
	        dirEntry.getFile('/'+folder+'/'+filename, { create: true }, function (fileEntry) {

	        	fileEntry.file(function (file) {

	                var readFile = new FileReader();

	        		readFile.onloadend = function (e) {

	                	var existingData = this.result;
	                	console.log('customer existing data',existingData);

	                	var dataToWrite = filedata;

	                	if(update) {
	                		console.log('data to update',filedata)
	                		var custData = filedata.split(fieldSep);
							var cardId = custData[0];

							var existingArray = existingData.split(rowSep);

							var idx=[];
							
							$.each(existingArray, function(index, elem) {
								   var custArr = elem.split(fieldSep);
								   if(custArr[0] == cardId){
								   		idx.push(index);
								   }
								});
					
							existingArray.splice(idx,1);

							var renewData = existingArray.join(rowSep);

							dataToWrite = renewData+rowSep+filedata;

	                	} else if(deletestatus){	         
							var cardId = filedata;

							var existingArray = existingData.split(rowSep);

							var idx=[];
							
							$.each(existingArray, function(index, elem) {
								   var custArr = elem.split(fieldSep);
								   if(custArr[0] == cardId){
								   		idx.push(index);
								   }
								});
					
							existingArray.splice(idx,1);

							var renewData = existingArray.join(rowSep);

							window.customerArray = renewData;

							dataToWrite = renewData;
	                	}else {
	                		if( $.trim(existingData) != "" ) {
	                			dataToWrite = existingData+rowSep+filedata;
	                		}

	                	}
	                	console.log(' dataToWrite ' + dataToWrite);

	                	//console.log(dataToWrite);
	                	if(!readonly){

		                	fileEntry.createWriter(function (fileWriter) {
		                		fileWriter.truncate(0);
			                	setTimeout(function () {
			                	 	fileWriter.onwriteend = function (e) {
			                        //write completed
				                    };

				                    fileWriter.onerror = function (e) {
				                        console.log('Write failed: ' + e.toString());
				                    };

				                    // Create a new Blob and write it
				                    var blob = new Blob([dataToWrite], { type: 'text/plain' }); //singleImageSrc], {type: imageType});
				                    fileWriter.write(blob);

				                   return callback(dataToWrite);

			                	}, 500);

		                	}, fsErrorHandler);

	                	} else {

	                		return callback(dataToWrite);

	                	}

	                };
	                readFile.readAsText(file);
	        	});
	        }, fsErrorHandler);
	    });

 	}
 	//End of readwriteentry function...

 	// function to store all user/customer transaction data into filesystem table
 	function readWriteTransactionEntry(folder, filename, filedata, readonly, update, callback){

 		// Create Folder
	    appFS.root.getDirectory(folder, { create: true }, function (dirEntry) {
	    	// Write settings to File System
	        dirEntry.getFile('/'+folder+'/'+filename, { create: true }, function (fileEntry) {

	        	fileEntry.file(function (file) {

	                var readFile = new FileReader();

	        		readFile.onloadend = function (e) {

	                	var existingData = this.result;
	                	//console.log('customer existing data',existingData);

	                	var dataToWrite = filedata;

	                	
	                	if(update) {
	                		//do something here
	                		dataToWrite = existingData+rowSep+filedata;
	                		console.log('transaction data update',dataToWrite);

	                		var txnData = filedata.split(fieldSep);
							var cardId = txnData[0];
							var rechargeCredit = txnData[2];

							var cardHolder = customerArray[cardId];
							

							var singleCustomerArray = cardHolder.split(fieldSep);
							
							var creditIdx = 5;

							singleCustomerArray[creditIdx] = parseInt(singleCustomerArray[creditIdx]) + parseInt(rechargeCredit);

							var updSingleCustData = singleCustomerArray.join(fieldSep);

							readWriteCustomerEntry(customerFolder, customerFile, updSingleCustData, false, true,false, writeDoneCallback);

	                	} else {
	                		if( $.trim(existingData) != "" ) {
	                			dataToWrite = existingData+rowSep+filedata;
	                		}

	                		if($.trim(filedata) != ""){
	                			// console.log('txn data to update',filedata)
		                		var txnData = filedata.split(fieldSep);
								var cardId = txnData[0];
								var deductCredit = txnData[2];

								var cardHolder = customerArray[cardId];
								

								var singleCustomerArray = cardHolder.split(fieldSep);
								
								var creditIdx = 5;

								singleCustomerArray[creditIdx] = singleCustomerArray[creditIdx] - deductCredit;

								var updSingleCustData = singleCustomerArray.join(fieldSep);

								readWriteCustomerEntry(customerFolder, customerFile, updSingleCustData, false, true,false, writeDoneCallback);
	                		}	         
	                	}
	                	
	                	//console.log(dataToWrite);
	                	if(!readonly){	                		

		                	fileEntry.createWriter(function (fileWriter) {
		                		fileWriter.truncate(0);
			                	setTimeout(function () {
			                	 	fileWriter.onwriteend = function (e) {
			                        //write completed
				                    };

				                    fileWriter.onerror = function (e) {
				                        console.log('Write failed: ' + e.toString());
				                    };

				                    // Create a new Blob and write it
				                    var blob = new Blob([dataToWrite], { type: 'text/plain' }); //singleImageSrc], {type: imageType});
				                    fileWriter.write(blob);

				                   return callback(dataToWrite);

			                	}, 500);

		                	}, fsErrorHandler);

	                	} else {

	                		return callback(dataToWrite);

	                	}

	                };
	                readFile.readAsText(file);
	        	});
	        }, fsErrorHandler);
	    });

 	}
 	//End of readwriteentry function...


 	//Call back after reading or writing...
	function writeDoneCallback(data){
		//after write is complete do whatever needed...

		customerTable.clear().draw();

		if( data != "" ) {
			var dataArray = data.split(rowSep);


			for(var i=0; i< dataArray.length; i++){

				var customerRow = dataArray[i].split(fieldSep);

				if( customerRow.length > 1 && $.trim(customerRow[0]) != ""){
					customerArray[customerRow[0]] = dataArray[i];

					//Putting in datatable
					customerTable.row.add([
						i+1,
						customerRow[1],
						customerRow[2],
						customerRow[0],
						customerRow[3],
						customerRow[5],
						customerRow[7],
						customerRow[8],
						'<div class="dropdown"><button id="dLabel" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Action<span class="caret"></span></button><ul class="dropdown-menu" role="menu" aria-labelledby="dLabel"><li><button  data-cardID="'+customerRow[0]+'" class="btn btn-primary btn-xs custTransaction" ><span class="glyphicon glyphicon-th-list"></span>See Transaction</button></li><li><button class="btn btn-primary btn-xs custEdit" data-title="Edit" data-toggle="modal" data-target="#regModal" data-cardID="'+customerRow[0]+'" ><span class="glyphicon glyphicon-pencil"></span>Edit</button></li><li><button class="btn btn-primary btn-xs custDelete" data-title="delete" data-cardID="'+customerRow[0]+'" ><span class="glyphicon glyphicon-trash"></span>Delete</button></li></ul></div>',
					]).draw(false);
				
				}

			}
				
				$('#regNewCustomer').trigger('reset');
				$('#newCustInfoForm').hide();
				$('#cardId').attr('disabled',false);
				$("#sendotpMsg").html('');
				$("#otpMsg").html('');
				$("#otpForm").hide();
				$(".custLoader").css('display','none');
				$('#regModal').modal('hide');
			return true;
		} else {
			return false;
		}
	}

	//customer Close
	$('body').on('click','.regNewCustomerClose',function(){
		$('#regModal').modal('hide');
		$('#regNewCustomer').trigger('reset');
		$('.newCustInfoForm').hide();
		$('#cardId').attr('disabled',false);
		$("#sendotpMsg").html();
		$("#otpMsg").html();
		$("#otpForm").hide();
		$("#sendOTP").attr('disabled',true);
		$(".custLoader").css('display','none');
	});

	//customer Edit function
	$('body').on('click', '.custEdit', function(){
		window.currentCardId  	= $(this).attr("data-cardid");
		var mode  				= $(this).attr("data-title");

		var customerRow = window.customerArray[window.currentCardId].split(fieldSep);

		console.log(customerRow);
		$(".newCustInfoForm").show();
		$("#sendOTP").hide();
		$("#cardId").val(customerRow[0]).attr('disabled',true);
		$("#idCard").val(customerRow[0]);
		$("#mode").val(mode);
		$("#firstName").val(customerRow[1]);
		$("#lastName").val(customerRow[2]);
		$("#email").val(customerRow[3]);
		$("#birthDate").val(customerRow[4]);
		// $("#phoneNumber").val(customerRow[5]);
		$("#credits").val(customerRow[5]).attr('readonly',true);
		$("#c_address").val(customerRow[6]);
		$("#expDate").val(customerRow[7]);
		$("#aniversaryDate").val(customerRow[8]);

	});

	$('body').on('click', '.custClose', function(){
		$('#regModal').modal('hide');
		$('#regNewCustomer').trigger('reset');
		$('#cardId').attr('disabled',false);
		$("#credits").attr('readonly',false);
	});

	
//customer Delete function
	$('body').on('click', '.custDelete', function(){
		window.currentCardId  	= $(this).attr("data-cardid");
		var mode  				= $(this).attr("data-title");


		if (confirm('Are you sure want to delete this record?')) {
			readWriteCustomerEntry(customerFolder, customerFile, window.currentCardId, false, false,true, writeDoneCallback);
		}
	});

	//service Edit function
	$('body').on('click', '.serviceEdit', function(){
		window.serviceName  	= $(this).attr("data-name");
		window.serviceMode  = $(this).attr("data-title");

		var serviceCredit = window.serviceArray[window.serviceName];

		console.log(serviceCredit,window.serviceName);
		window.editServiceFlag = window.serviceName;
		$("#service_name").val(window.serviceName);
		$("#credit_point").val(serviceCredit);
		$("#serviceMode").val(window.serviceMode);
		
	});

	//Service Delete function
	$('body').on('click', '.serviceDelete', function(){
		window.serviceName  	= $(this).attr("data-name");
		window.serviceMode  = $(this).attr("data-title");
		if (confirm('Are you sure want to delete this record?')) {
			readWriteServiceEntry(serviceFolder, serviceFile, window.serviceName, false, false,true, writeDoneServiceCallback);
		}
	});


	//callback after transaction entry
	function writeDoneTrasactionCallback(data){
		//after write is complete do whatever needed...

		transactionTable.clear().draw();

		if( data != "" ) {
			var dataArray = data.split(rowSep);


			for(var i=0; i< dataArray.length; i++){

				var transactionRow = dataArray[i].split(fieldSep);
console.log('transactionRow',transactionRow);
				if( transactionRow.length > 1 && $.trim(transactionRow[0]) != ""){

					var t_type = "Credited";
					if($.trim(transactionRow[1]) != "Recharged" ) {
						t_type = "Debited";
					}
					//Putting in datatable
					transactionTable.row.add([
						i+1,
						transactionRow[0],
						transactionRow[1],
						transactionRow[2],
						t_type,
						transactionRow[3]
					]).draw(false);
				}

			}
			$('#rechargeCardModal').modal('hide');
			$('#rechargeCredit').trigger("reset");
			$('.recharge_screen_customer_detail').hide();
			$('#recharge_credit_btn').hide();
			
			return true;
			
		} else {
			return false;
		}
	}

	//Call back after reading or writing...
	function creditScreenData(data){
		//after write is complete do whatever needed...

		var singleEntry = [];

		if( data != "" ) {
			var dataArray = data.split(rowSep);

			for(var i=0; i< dataArray.length; i++){

				var customerRow = dataArray[i].split(fieldSep);

				if( window.currentCardId == customerRow[0] ) {
					singleEntry = customerRow;
					break;
				}
			}
			console.log('single entry log',singleEntry);
			if( singleEntry.length > 0 ) {

				var expDate=singleEntry[7];
				console.log('single customer data',singleEntry)
				expDate=expDate.split("-");

				var newExpDate=expDate[1]+"/"+expDate[0]+"/"+expDate[2];
				var expTimestamp = "";
				expTimestamp = new Date(newExpDate).getTime(); //1330210800000

				var todayDate = currentDate; 
				//console.log(todayDate)
				todayDate=todayDate.split("-");
				var newTodayDate=todayDate[1]+"/"+todayDate[0]+"/"+todayDate[2];
				var todayTimestamp = "";
				todayTimestamp = new Date(newTodayDate).getTime(); //1330210800000

				window.currentTotalCredit = singleEntry[5];
				console.log('creditScreenData',window.currentTotalCredit);
				$('#credit_screen_name').html(singleEntry[1]+" "+singleEntry[2]);
				$('#credit_screen_phone').html(singleEntry[0]);
				$('#credit_screen_address').html(singleEntry[6]);
				$('#credit_screen_credits').val(singleEntry[5]);
				$('#deduct_card_id').val(singleEntry[0]);

				$('.credit_screen_customer_detail').show();
				$('#deduct_credit_btn').show();
				$('#resetInvoiceForm').show();
 				$('.ui_blocker_credit_screen').hide();
 				$('.expMsg').html("").hide();
 				$('.service-taken').show();
				$('.deduct-credit').show();
				$('.ui_blocker_credit_screen_error').hide().html('');
				if(todayTimestamp > expTimestamp ){
					$('.service-taken').hide();
					$('.deduct-credit').hide();
					$('.expMsg').html("Your card amount is expired.").show();
					$('#deduct_credit_btn').hide();
				}	
			}else{
 				$('.ui_blocker_credit_screen').hide();
 				$('.ui_blocker_credit_screen_error').show().html('No records found!!');
			}

			

			return true;
			
		} else {
			$('.ui_blocker_credit_screen').hide();
			return false;
		}
	}

	//Call back after reading or writing recharge...
	function rechargeScreenData(data){
		//after write is complete do whatever needed...

		var singleEntry = [];

		if( data != "" ) {
			var dataArray = data.split(rowSep);

			for(var i=0; i< dataArray.length; i++){

				var customerRow = dataArray[i].split(fieldSep);

				if( window.currentCardId == customerRow[0] ) {
					singleEntry = customerRow;
					break;
				}
			}

			if( singleEntry.length > 0 ) {

				
				window.currentTotalCredit = singleEntry[5];
				$('#recharge_screen_name').html(singleEntry[1]+" "+singleEntry[2]);
				$('#recharge_screen_phone').html(singleEntry[0]);
				$('#recharge_screen_address').html(singleEntry[6]);
				$('#recharge_screen_credits').val(singleEntry[5]);
				$('#recharge_card_id').val(singleEntry[0]);
				$('.recharge_screen_customer_detail').show();
				$('#recharge_credit_btn').show();
				$('.recharge_credit').show();

				$('.ui_blocker_credit_screen').hide();
 				$('.ui_blocker_credit_screen_error').hide().html('');
				
				
			}else{
 				$('.ui_blocker_credit_screen').hide();
 				$('.ui_blocker_credit_screen_error').show().html('No records found!!');
			}

			

			return true;
			
		} else {
			return false;
		}
	}

	//Add new customer button action
	$("body").on('click','#addNew',function(){
		$(".newCustInfoForm").hide();
		$("#otpMsg").html("");
		$("#sendotpMsg").html("");
		$("#cardId").attr('readonly',false);
		$("#sendOTP").show();
		$("#sendOTP").attr('disabled',true);
	});

	//Getting new customer/user details to create entry in table
	$("form[name='regNewCustomer']").validate({
		 rules: {
		 		cardId:{
		 				required:true,
		 				unique_value_card:true,
		 		},
		 		firstName: {
				        required: true,
				        },
				lastName: {
				        required: true,
				        },
				
				credits: {
						required: true,
				}
		  		
		    },
		    submitHandler: function(form) {
			   	var formData = $("#regNewCustomer").serializeToJSON();
			   	var card = formData.cardId;
			   	var update = false;
			   	console.log('newcustomer data',formData);
			   	var filedata = card+fieldSep+formData.firstName+fieldSep+formData.lastName+fieldSep+formData.email+fieldSep+formData.birthDate+fieldSep+formData.credits+fieldSep+formData.c_address+fieldSep+formData.expDate+fieldSep+formData.aniversaryDate;

				if(formData.mode){
					card = formData.idCard
					update = true;
					filedata = card+fieldSep+formData.firstName+fieldSep+formData.lastName+fieldSep+formData.email+fieldSep+formData.birthDate+fieldSep+formData.credits+fieldSep+formData.c_address+fieldSep+formData.expDate+fieldSep+formData.aniversaryDate;
				}
				
				readWriteCustomerEntry(customerFolder, customerFile, filedata, false, update,false, writeDoneCallback);
				
			}	
	});

	//Send OTP function to verify mobile number
	$('body').on('click',".sendOTP",function(){
		$("#sendotpMsg").html("");
		$(".sendOTP").show();
		$(".custLoader").css('display','block');
		var mobileNo 	= $("#cardId").val();
		var otp 		= Math.floor(1000 + Math.random() * 9000);
		if(mobileNo){
			$.ajax({
				type:'post',
				url:"http://vkaps.com/chrome_extension_backup/sms_api.php",
				data: {"data_type":"sendotp","phone":mobileNo,"otp":otp},
				error: function(jqXHR) { 
			    if(jqXHR.status==0) {
			            //$('#backupRespMsg').html("There is weak or No internet connection, Please try again!!").css('color','red');
			        }
			    },
				success:function(respData) {	
					if(respData != ''){
						window.otp = parseInt(respData);
						$(".custLoader").css('display','none');
						$("#sendotpMsg").html("OTP Sent!!");
						$("#otpForm").show();
					}else{
						console.log('no respData');
					}
				}
			});
		}
		
		
	});

	//verify OTP function to verify otp number
		$('body').on('click',"#verifyOTP",function(){
			var verifyOTP = $("#otpNum").val();
			// console.log(verifyOTP,typeof(verifyOTP));
			// console.log(window.otp,typeof(window.otp));
			if(window.otp == parseInt(verifyOTP)){
				$(".newCustInfoForm").show();
				$("#otpMsg").html("OTP verified").css('color','green');
				$("#sendotpMsg").html("");
				$(".sendOTP").hide();
				$("#otpForm").fadeOut(1600);
				$("#cardId").attr('readonly',true);
			}else{
				$("#otpMsg").html("OTP is not correct").css('color','red');
			}
			

		});

	//profile form entry
	$("form[name='profileForm']").validate({
		 rules: {
		 		business_name:{
		 			required:true,
		 		},
		 		business_mobile: {
		        	required: true,
		        }
		  		
		    },
		    submitHandler: function(form) {
			   	var formData = $("#profileForm").serializeToJSON();
			   	if(formData){
					localStorage.setItem('business_name', formData.business_name);
					localStorage.setItem('business_mobile', formData.business_mobile);
					localStorage.setItem('business_address', formData.business_address);
					$('.logo').html("<span>"+formData.business_name+"</span>");
				}
				$('.saveprofileMsg').html('Profile info saved successfully!!').css('color','green').fadeOut(5000)
			}
		});

	//Getting new trasactions detail (credit decution)
	$("form[name='deductCredit']").validate({
		 rules: {
		 		deduct_credit:{
		 			required:true,
		 			lessThanEqual:true,
		 		},
		 		service_taken: {
		        	required: true,
		        }
		  		
		    },
		    submitHandler: function(form) {
			   	var formData = $("#deductCredit").serializeToJSON();
				

				var filedata = formData.deduct_card_id+fieldSep+formData.service_taken+fieldSep+formData.deduct_credit+fieldSep+currentDate;



				readWriteTransactionEntry(transactionFolder, transactionFile, filedata, false, false, writeDoneTrasactionCallback);
				
				if(formData.deduct_card_id){
					$.ajax({
						type:'post',
						url:"http://vkaps.com/chrome_extension_backup/sms_api.php",
						data: {"data_type":"transaction","phone":formData.deduct_card_id,"service":formData.service_taken,"deduct_credit":formData.deduct_credit,'last_credit':formData.remainingCredit,'business_name':localStorage.getItem('business_name')},
						error: function(jqXHR) { 
					    if(jqXHR.status==0) {
					            //$('#backupRespMsg').html("There is weak or No internet connection, Please try again!!").css('color','red');
					        }
					    },
						success:function(respData) {	
							if(respData != ''){
								
							}else{
								console.log('no respData');
							}
						}
					});
				}

				$('#invoice').modal('hide');
				$('#deductCredit').trigger("reset");
				$('.credit_screen_customer_detail').hide();
				$('#deduct_credit_btn').hide();
			}	
	});

	//Reset deduct credit form of close or submit of form
	$('.deductCreditClose').click(function(){
		$("#deductCredit").trigger("reset");
		$('.credit_screen_customer_detail').hide();
		$('#deduct_credit_btn').hide();

	});

	//Getting new trasactions detail (credit decution)
	$("form[name='rechargeCredit']").validate({
		 rules: {
		 		recharge_credit:{
		 			required:true,		 			
		 		}		 			  
		    },
		    submitHandler: function(form) {
			   	var formData = $("#rechargeCredit").serializeToJSON();
				

				var filedata = formData.recharge_card_id+fieldSep+"Recharged"+fieldSep+formData.recharge_credit+fieldSep+currentDate;

				readWriteTransactionEntry(transactionFolder, transactionFile, filedata, false, true, writeDoneTrasactionCallback);
			}	
	});

	//Reset recharge credit form of close or submit of form
	$('.rechargeCardClose').click(function(){
		$("#rechargeCredit").trigger("reset");
		$('.recharge_screen_customer_detail').hide();
		$('#recharge_credit_btn').hide();

	});

	//Reset recharge credit form of close or submit of form
	$('.rechargeCard').click(function(){
		$("#rechargeCredit").trigger("reset");
		$("#recharge_screen_card_id").val('')

	});

	//Get individual customer history by card id
	$('body').on('click', '.custTransaction', function(){

	  window.currentCardId  = $(this).attr("data-cardid");

	  $('#trans_customer_card').text(window.currentCardId);

	  var customerRow = window.customerArray[window.currentCardId].split(fieldSep);

	  $('#trans_customer_name').text(customerRow[1]+" "+customerRow[2]);

	  $('#trans_customer_credit').text(customerRow[5]);


	  readWriteTransactionEntry(transactionFolder, transactionFile, "", true, false, writeDoneCustomerTrasactionCallback);

	  $("#serviceScreen").hide();
 		$("#dashboardScreen").hide();
 		$("#transactionScreen").hide();
 		$("#customerTransactionScreen").show();

	});

	//callback for individual customer transaction history
	function writeDoneCustomerTrasactionCallback(data){

		customerTransactionTable.clear().draw();

		if( data != "" ) {
			var dataArray = data.split(rowSep);

			window.totalRechargeCredit = 0;
			for(var i=0; i< dataArray.length; i++){

				var transactionRow = dataArray[i].split(fieldSep);

				if( transactionRow.length > 1 && $.trim(transactionRow[0]) == window.currentCardId){

					var t_type = "Credited";
					if($.trim(transactionRow[1]) != "Recharged" ) {
						t_type = "Debited";
					}else{
						 window.totalRechargeCredit = parseInt(window.totalRechargeCredit) + parseInt(transactionRow[2]);
						 console.log('total recharge',window.totalRechargeCredit);
					}


					//Putting in datatable
					customerTransactionTable.row.add([
						i+1,
						transactionRow[1],

						transactionRow[2],
						t_type,
						transactionRow[3]
					]).draw(false);
				}

			}

			$("#trans_customer_total_recharge").text(window.totalRechargeCredit);
			return true;
			
		} else {

			return false;
		}
	}


	//CODE FOR SERVICES CRUD

	//Global function to store all user/customer data into filesystem table
 	function readWriteServiceEntry(folder, filename, filedata, readonly, update,deleteStatus, callback){

 		console.log(' here 1',folder,filename,filedata,readonly,update);

 		// Create Folder
	    appFS.root.getDirectory(folder, { create: true }, function (dirEntry) {
	    	console.log('service here 2');
	        // Write settings to File System
	        dirEntry.getFile('/'+folder+'/'+filename, { create: true }, function (fileEntry) {

	        	fileEntry.file(function (file) {

	                var readFile = new FileReader();


	        		readFile.onloadend = function (e) {

	                	var existingData = this.result;
	                	console.log('service existing',existingData);

	                	var dataToWrite = filedata;
	                	if(update) {
	                		// console.log('data to update',filedata)
	                		var servData = filedata.split(fieldSep);
							var servName = editServiceFlag;
							// console.log("serviceName", servName);
							var existingArray = existingData.split(rowSep);
							// console.log('existing service',existingArray)
							var idx=[];

							$.each(existingArray, function(index, elem) {
								   var servArr = elem.split(fieldSep);
								   if(servArr[0] == servName){
								   		idx.push(index);
								   }
								});
					
							existingArray.splice(idx,1);

							// console.log('existing service',existingArray)

							var renewData = existingArray.join(rowSep);

							dataToWrite = renewData+rowSep+filedata;
	                	} else if(deleteStatus) {
	                		// console.log('data to update',filedata)
	                		
							var servName = filedata;
							// console.log("serviceName", servName);
							var existingArray = existingData.split(rowSep);
							// console.log('existing service',existingArray)
							var idx=[];

							$.each(existingArray, function(index, elem) {
								   var servArr = elem.split(fieldSep);
								   if(servArr[0] == servName){
								   		idx.push(index);
								   }
								});
					
							existingArray.splice(idx,1);

							// console.log('existing service',existingArray)

							var renewData = existingArray.join(rowSep);
							window.serviceArray = existingArray;
							dataToWrite = renewData;
	                	}else {

	                		if( $.trim(existingData) != "" ) {
	                			dataToWrite = existingData+rowSep+filedata;
	                		}
	                		console.log('service service add',dataToWrite);
	                	}
	                	
	                	if(!readonly){

		                	fileEntry.createWriter(function (fileWriter) {

		                		fileWriter.truncate(0)
			                	setTimeout(function () {
			                	 	fileWriter.onwriteend = function (e) {
			                        //write completed
				                    };

				                    fileWriter.onerror = function (e) {
				                        console.log('Write failed: ' + e.toString());
				                    };

				                    // Create a new Blob and write it
				                    var blob = new Blob([dataToWrite], { type: 'text/plain' }); //singleImageSrc], {type: imageType});
				                    fileWriter.write(blob);

				                   return callback(dataToWrite);

			                	}, 500);

		                	}, fsErrorHandler);

	                	} else {

	                		return callback(dataToWrite);

	                	}

	                };
	                readFile.readAsText(file);
	        	});
	        }, fsErrorHandler);
	    });

 	}
 	//End of readwriteentry function...

 	//Call back After Service add 
	function writeDoneServiceCallback(data){
		//after write is complete do whatever needed...
		$('#service_taken').html('');
		$('#service_taken').append('<option value="" >Select Service</option>');

		serviceTable.clear().draw();

		if( data != "" ) {
			var dataArray = data.split(rowSep);


			for(var i=0; i< dataArray.length; i++){

				var serviceRow = dataArray[i].split(fieldSep);



				if( serviceRow.length > 1 && $.trim(serviceRow[0]) != ""){
					$('#service_taken').append('<option value="'+serviceRow[0]+'" >'+serviceRow[0]+' ('+serviceRow[1]+')</option>');

					serviceArray[serviceRow[0]] = serviceRow[1];
					//Putting in datatable
					serviceTable.row.add([
						i+1,
						serviceRow[0],
						serviceRow[1],
						'<button class="btn btn-primary btn-xs serviceEdit" data-title="Edit" data-toggle="modal" data-target="#serviceModal" data-name="'+serviceRow[0]+'" ><span class="glyphicon glyphicon-pencil"></span></button>',
						'<button class="btn btn-primary btn-xs serviceDelete" data-name="'+serviceRow[0]+'" data-title="delete" ><span class="glyphicon glyphicon-trash"></span></button>',
					]).draw(false);

					$('#service_taken').selectpicker('refresh');
					console.log(' Ocean ');
				}
			

			}
			
			$('#service_taken').selectpicker('refresh');
			
			return true;
			
		} else {
			return false;
		}
	}
	//Call back After Service add 


	//Getting new entry for services
	$("form[name='newServiceForm']").validate({
		 rules: {
		 		
		 		service_name: {
				        required: true,
				        unique_value: true,
				        },
				credit_point: {
				        required: true,
				        }
		  		
		    },
		    submitHandler: function(form) {
			   	var formData = $("#newServiceForm").serializeToJSON();
				console.log(formData);

				var updateService = false;
				var filedata = formData.service_name+fieldSep+formData.credit_point;

				if(formData.mode){
					updateService = true;
					filedata = formData.service_name+fieldSep+formData.credit_point;
				}

				readWriteServiceEntry(serviceFolder, serviceFile, filedata, false, updateService, false,writeDoneServiceCallback);
				$('#serviceModal').modal('hide');
				$('#newServiceForm').trigger("reset");
			}	
	});


	//fetch all services and show into table
	setTimeout(function () {
		readWriteCustomerEntry(customerFolder, customerFile, "", true, false,false, writeDoneCallback);
		readWriteServiceEntry(serviceFolder, serviceFile, "", true, false,false, writeDoneServiceCallback);
		readWriteTransactionEntry(transactionFolder, transactionFile, "", true, false, writeDoneTrasactionCallback);
	}, 400);

	//END OF SERVICES CRUD

	// backup data on server using sync button
	$("body").on('click','#syncBackup', function(){

		//console.log('customer ajax aya');
		$('#backupRespMsg').html();
		if (customerTable.data().any() ) {
		   readWriteCustomerEntry(customerFolder, customerFile, "", true, false,false, writeDoneCustomerBackupCallback);
		}else{
			$('#backupRespMsg').html('No Customer Data to sync backup').css('color','red');
			$('.backup_loader').css("display","none");
		}

		if (serviceTable.data().any() ) {
		  readWriteServiceEntry(serviceFolder, serviceFile, "", true, false,false, writeDoneServiceBackupCallback);
		}else{
			$('#backupRespMsg').html('No Service Data to sync backup').css('color','red');
			$('.backup_loader').css("display","none");
		}
		
		
		if (transactionTable.data().any() ) {
		  readWriteTransactionEntry(transactionFolder, transactionFile, "", true, false, writeDoneTrasactionBackupCallback);
		}else{
			$('#backupRespMsg').html('No Transaction Data to sync backup').css('color','red');
			$('.backup_loader').css("display","none");
		}

	});

	function writeDoneCustomerBackupCallback(data){
		if(data != ''){
			$.ajax({
				type:'post',
				url:"http://vkaps.com/chrome_extension_backup/get_request.php",
				data: {"data_type":"customers","name":window.name,"phone":window.phone,"data":data},
				error: function(jqXHR) { 
			    if(jqXHR.status==0) {
			            $('#backupRespMsg').html("There is weak or No internet connection, Please try again!!").css('color','red');
			        }
			    },
				success:function(respData) {	
					console.log('customer response',respData)
					if(respData != ''){
						$('.backup_loader').css("display","none");
						$('#backupRespMsg').html("Backup synced successfully!!");
						
					}else{
						$('#backupRespMsg').html("There is weak or No internet connection, Please try again!!");
					}
				}
			});
		}
	}

	function writeDoneServiceBackupCallback(data){
		if(data != ''){
			$.ajax({
				type:'post',
				url:"http://vkaps.com/chrome_extension_backup/get_request.php",
				data: {"data_type":"service","name":window.name,"phone":window.phone,"data":data},
				error: function(jqXHR) { 
			    if(jqXHR.status==0) {
			            $('#backupRespMsg').html("There is weak or No internet connection, Please try again!!").css('color','red');
			        }
			    },
				success:function(respData) {
				console.log('service response',respData)	
					if(respData != ''){
						$('.backup_loader').css("display","none");
						$('#backupRespMsg').html("Backup synced successfully!!");
						
					}else{
						$('#backupRespMsg').html("There is weak or No internet connection, Please try again!!");
					}
				}
			});
		}
	}

	function writeDoneTrasactionBackupCallback(data){
		if(data != ''){
			$.ajax({
				type:'post',
				url:"http://vkaps.com/chrome_extension_backup/get_request.php",
				data: {"data_type":"transaction","name":window.name,"phone":window.phone,"data":data},
				error: function(jqXHR) { 
			    if(jqXHR.status==0) {
			            $('#backupRespMsg').html("There is weak or No internet connection, Please try again!!").css('color','red');
			        }
			    },
				success:function(respData) {
				console.log('txn response',typeof(respData));		
					if(respData != ''){
						$('.backup_loader').css("display","none");
						$('#backupRespMsg').html("Backup synced successfully!!").css('color','green');;
						
					}else{
						$('#backupRespMsg').html("There is weak or No internet connection, Please try again!!").css('color','red');;
					}
				}
			});
		}
	}
	
});